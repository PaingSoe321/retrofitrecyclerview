package gg.com;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GalleryActivity extends AppCompatActivity {
    private static final String TAG = "GalleryActivity";

    private List<RetroPhoto> dataList;
    private Context context;
    private ImageView image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        Log.d(TAG, "onCreate: started.");

        image = (ImageView) findViewById(R.id.image);





        Picasso.Builder builder = new Picasso.Builder(GalleryActivity.this);
        builder.downloader(new OkHttp3Downloader(GalleryActivity.this));
        builder.build().load("https://via.placeholder.com/150/24f355")
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(image);


    }


}
